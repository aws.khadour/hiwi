import os
from os.path import join

import cv2
import yaml

""" Specify which images of which cameras should by used for cropping. """
CAMERAS_TO_READ = ["ee_fuerholzenw17_0", "nn_fuerholzenost4_0", "ss_fuerholzenost13", "s_fuerholzenw5_0",
                   "sw_fuerholzenw9"]
"""
Only images with the given hour will be read. Only full hours will be considered.
Example: 2021-04-01T15-00_CAMNAME.jpg will be read, if only '15' is in this list, but 2021-04-01T15-05_CAMNAME.jpg and
2021-04-01T16-00_CAMNAME.jpg will be ignored.
"""
HOURS_TO_READ = ["04", "06", "08", "10", "12", "14", "16", "18", "20"]
# HOURS_TO_READ = ["00", "04", "08", "12", "16", "20"]
# HOURS_TO_READ = ["05", "08", "13", "16", "20"]
""" Images with a width smaller than the given value will be ignored. """
MIN_IMAGE_WIDTH = 0
""" Set the cropping style: Square vs. rectangular. """
LABEL_STYLE = ["square", "rect"][0]

""" Path to the directory that contains the sub directories for full images and cropped images that will be created. """
DATA_DIR_PATH = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/02_images"
""" Path to the full images. """
IMAGE_DIR_PATH = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/02_images/full/2021"
""" Path to the directory in which cropped images whould be saved. """
TARGET_PATH = join(DATA_DIR_PATH, f"cropped_{LABEL_STYLE}/")
""" Path to the YAML that contains the parameters for the cropping (location, width, height, rotation). """
PARAMS_PATH = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/03_implementation/image_cropping/crop_params/parking_lot_crop_params_square.yaml"


def write_cropped_images(image, image_name, params, min_image_width):
    year_dir = image_name.split("-")[0]
    month_dir = year_dir + "-" + image_name.split("-")[1]
    if not os.path.isdir(TARGET_PATH):
        os.mkdir(TARGET_PATH)
    if not os.path.isdir(join(TARGET_PATH, year_dir)):
        os.mkdir(join(TARGET_PATH, year_dir))
    if not os.path.isdir(join(TARGET_PATH, year_dir, month_dir)):
        os.mkdir(join(TARGET_PATH, year_dir, month_dir))

    for id, cropped_image in enumerate(get_cropped_images(image, image_name, params, min_image_width)):
        cv2.imwrite(join(TARGET_PATH, year_dir, month_dir, f"{image_name.replace('.jpg', '')}_{id}.jpg"), cropped_image)


def get_cropped_images(image, image_name, params, min_image_width):
    cropped_images = []
    parking_name = "_".join(image_name.split("_")[1:]).replace(".jpg", "")
    for i in range(len(params[parking_name]["geometry"])):
        w = params[parking_name]["geometry"][i]["width"]
        h = params[parking_name]["geometry"][i]["height"]
        x = params[parking_name]["geometry"][i]["x"]
        y = params[parking_name]["geometry"][i]["y"]
        angle = params[parking_name]["geometry"][i]["angle"]
        if params[parking_name]["use_center_of_rec"] == "true":
            x -= int(w / 2)
            y -= int(h / 2)
        rotation_matrix = cv2.getRotationMatrix2D((x + int(w / 2), y + int(h / 2)), angle, 1)
        image_rot = cv2.warpAffine(image, rotation_matrix, (image.shape[1], image.shape[0]))
        image_rot = image_rot[y:(y + h), x:(x + w)]
        if image_rot.shape[0] >= min_image_width:
            cropped_images.append(image_rot)

    return cropped_images


def clean_full_images():
    image_names = os.listdir(IMAGE_DIR_PATH + "/2021-05")
    for image_name in image_names:
        os.remove(IMAGE_DIR_PATH + "/2021-05/" + image_name)


def clean_cropped_square_images():
 image_names = os.listdir(TARGET_PATH + "/2021/2021-05")
 for image_name in image_names:
     os.remove(TARGET_PATH + "/2021/2021-05" + "/" + image_name)


def do_cropping():
    cropped_images = []

    with open(PARAMS_PATH) as file:
        params = yaml.load(file, Loader=yaml.FullLoader)
        sub_dirs = os.listdir(IMAGE_DIR_PATH)
        for sub_dir in sub_dirs:
            image_names = os.listdir(join(IMAGE_DIR_PATH, sub_dir))
            for image_name in image_names:
       # if any([s in image_name for s in CAMERAS_TO_READ]) and any(["T" + s + "-00" in image_name for s in HOURS_TO_READ]):
                image = cv2.imread(join(IMAGE_DIR_PATH, sub_dir, image_name))
                write_cropped_images(image, image_name, params, MIN_IMAGE_WIDTH)
                cropped_images.append(get_cropped_images(image,image_name,params,MIN_IMAGE_WIDTH))


    return cropped_images