import os
from os.path import join, abspath, dirname

import cv2
import numpy as np
import tensorflow as tf
from crop_images import *

IMAGE_SIZE = (75, 75)

""" Path to the directory that contains the sub directories for full images and cropped images that will be created. """
DATA_DIR_PATH = join(dirname(abspath(__name__)), "02_images")
""" Path to the full images. """
IMAGE_DIR_PATH = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/02_images/cropped_square/2021/2021-05/"
""" path to the lite Model to load """
LITE_MODEL_PATH = join(dirname(abspath(__name__)), "04_lite_models")

MODEL_TO_LOAD = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/04_lite_models/DenseNet121_(75, 75)size_112batch_13aug_0tta__square_quant_new_v2.tflite"


# read images to predict from specific Path

def read_images(image_dir_path, image_size, min_width_of_images_to_read):
    images_found = []
    images = None
    for filename in os.listdir(image_dir_path):
        img = cv2.imread(os.path.join(image_dir_path, filename))
        if images is None and img.shape[1] >= min_width_of_images_to_read:
            image = cv2.resize(img, image_size)
            images = [image]
            images_found.append(True)
        elif img.shape[0] >= min_width_of_images_to_read:
            image = cv2.resize(img, image_size)
            images = np.concatenate([images, [image]])
            images_found.append(True)

    return images, images_found


# this Method is used to predict the class of the output of lite_model for the validation data_set
# the class can be either 0 "empty" or 1 "occupied"
# prediction_digits will be processed later to extract the accuracy of the validation data_set


def evaluate_model_lite(interpreter):
    input_index = interpreter.get_input_details()[0]["index"]
    output_index = interpreter.get_output_details()[0]["index"]
    # you schould add something hier
    do_cropping()

    images, images_found = read_images(IMAGE_DIR_PATH, IMAGE_SIZE, 50)

    clean_full_images()
    clean_cropped_square_images()
    prediction_digits = []

    if images is not None :
    # Run predictions on every image in the "test" dataset.
        for i, x_val_square_i in enumerate(images):
            if i % 1000 == 0:
                print('Evaluated on {n} results so far.'.format(n=i))
        # Pre-processing: add batch dimension and convert to float32 to match with
        # the model's input data format.

            x_val_square_i = np.expand_dims(x_val_square_i, axis=0).astype(np.uint8)
            interpreter.set_tensor(input_index, x_val_square_i)
            interpreter.invoke()
            output = interpreter.tensor(output_index)
            digit = np.argmax(output()[0])
            prediction_digits.append(digit)

    print('\n')

    prediction_digits = np.array(prediction_digits)

    return prediction_digits

def create_bit_array_and_save_result(prediction_digits):
    results = []
    for x in prediction_digits:
        if x == 0:
            results.append("0")
        if x == 1:
            results.append("1")

    results.append("000000")
    result_string = ''.join(results)
    result_string = int(result_string[::-1], 2).to_bytes(2, 'big')
    with open("C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/03_implementation/result/result.bin", mode='wb') as file:
              file.write(result_string)



def create_interpreter_and_do_prediction():
    # create interpreter to inference the model by load Quantized Lite Model
    interpreter = tf.lite.Interpreter(MODEL_TO_LOAD)
    # this necessary to inference the Model
    interpreter.allocate_tensors()
    # print the data-type of the input and Output of the Model that is already now implemented on The interpreter
    input_type = interpreter.get_input_details()[0]['dtype']
    print('input: ', input_type)
    output_type = interpreter.get_output_details()[0]['dtype']
    print('output: ', output_type)

    prediction_digits = evaluate_model_lite(interpreter)
    create_bit_array_and_save_result(prediction_digits)

    print(prediction_digits)

    return prediction_digits


if __name__ == '__main__':
    # create interpreter to inference the model by load Quantized Lite Model
    interpreter = tf.lite.Interpreter(MODEL_TO_LOAD)
    # this necessary to inference the Model
    interpreter.allocate_tensors()
    # print the data-type of the input and Output of the Model that is already now implemented on The interpreter
    input_type = interpreter.get_input_details()[0]['dtype']
    print('input: ', input_type)
    output_type = interpreter.get_output_details()[0]['dtype']
    print('output: ', output_type)

    prediction_digits = evaluate_model_lite(interpreter)
    create_bit_array_and_save_result(prediction_digits)

    print(prediction_digits)
