from tkinter import Tk, Label, Button, Text, END, Entry
from tkinter import filedialog
import shutil
import cv2
from PIL import Image
from PIL import ImageTk
from parking_prediction import *

IMAGE_LOAD_PATH = "C:/Users/Aws/Desktop/AIOT-MASTER-Project/Hiwi/02_images/full/2021/2021-05/"

def select_image():
    # grab a reference to the image panels
    global panelA
    # open a file chooser dialog and allow the user to select an input
    # image
    path = filedialog.askopenfilename()
    if len(path) > 0:
        # load the image from disk, convert it to grayscale, and detect
        # edges in it
        image = cv2.imread(path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # OpenCV represents images in BGR order; however PIL represents
        # images in RGB order, so we need to swap the channels
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        # convert the images to PIL format...
        image = Image.fromarray(image)
        # ...and then to ImageTk format
        image = ImageTk.PhotoImage(image)
        shutil.copy(path,IMAGE_LOAD_PATH)

        if panelA is None :
            # the first panel will store our original image
            panelA = Label(image=image)
            panelA.image = image
            panelA.pack(side="left", padx=10, pady=10)
            # while the second panel will store the edge map

        else:
            # update the pannels
            panelA.configure(image=image)
            panelA.image = image


def set_text_by_button():
    # Delete is going to erase anything
    # in the range of 0 and end of file,
    # The respective range given here
   prediction_digits = create_interpreter_and_do_prediction()
   panelB.delete(0,"end")
   panelB.insert(0,prediction_digits)


if __name__ == '__main__':
    root = Tk()
    panelB = Entry()
    panelB.pack(side="right", padx=10, pady=10)
    panelA = None
    # create a button, then when pressed, will trigger a file chooser
    # dialog and allow the user to select an input image; then add the
    # button the GUI
    btn = Button(root, text="Select an image", command=select_image)
    set_up_button = Button(root, height=1, width=10, text="run",
                                   command=set_text_by_button)
    btn.pack(side="bottom", fill="both", expand="yes", padx="10", pady="10")
    set_up_button.pack(side="bottom", fill="both", expand="yes", padx="5", pady="5")
    # kick off the GUI
    root.mainloop()