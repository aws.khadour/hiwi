# Usage of the parking-detection tool on Pi

# Requirements

please simply follow the following to install `tenserflow > 2.0 and all necessary dependencies` on The Pi :

 - Make your project directory:
   - cd Desktop
   - mkdir tf_pi
   - cd tf_pi
 
 
 - Make a virtual environment (I'm assuming you have Python 3):
   - python3 -m pip install virtualenv
   - virtualenv env
   - source env/bin/activate
 
 - sudo apt-get install -y libhdf5-dev libc-ares-dev libeigen3-dev
 - python3 -m pip install keras_applications==1.0.8 --no-deps
 - python3 -m pip install keras_preprocessing==1.1.0 --no-deps
 - python3 -m pip install h5py==2.9.0
 - sudo apt-get install -y openmpi-bin libopenmpi-dev
 - sudo apt-get install -y libatlas-base-dev
 - python3 -m pip install -U six wheel mock
 
 - Pick a tensorflow release from **https://github.com/lhelontra/tensorflow-on-arm/releases** (I picked 2.4.0): 
 - wget https://github.com/lhelontra/tensorfl...
 - python3 -m pip uninstall tensorflow
 - python3 -m pip install tensorflow-2.4.0-cp37-none-linux_armv7l.whl
 
 - python3 -m pip uninstall tensorflow
 - python3 -m pip install tensorflow-2.4.0-cp37-none-linux_armv7l.whl

**maybe may occur TimeOutException for the Last step , simply repeat it**


- RESTART YOUR TERMINAL
- Reactivate your virtual environment:
- cd Desktop
- cd tf_pi
- source env/bin/activate


- Test:
- Open a python interpreter by executing:  
- python3 
  - import tensorflow
  - tensorflow.__version__
  
------------------------------------------------------------------------------------------------------------------------

### the important Functions for main API

|         Function | Purpose                                                                                                         |
|-----------------:|:----------------------------------------------------------------------------------------------------------------|
|   evaluate_model_lite         | this Method is used to evaluate Model with Validation data|
|   read_images                 | this Method used to read the Images                       |                             



# Architecture of The repository

|           Foldername | Purpose                                                                                                                                                                                                                |
|---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------           |
| 00_Administration    | Folder contains meeting Information |
| 01_Documentation     | Folder contains  devdoc             |
| 02_02_images         | Folder Contains ctopped images      |                                                                       
| 03_implementation    | Folder Contains the Implementation  |                                                            
| 04_lite_models       | Folder contains the lite Models     |




 
## how to run this tool 

**"important Note"** please be sure to use  python-virtual Environment that is explained above how to install and use it **

- simply after that you have everything installed and read and  adapt the necessary paths in the main program `parking_prediction.py` run the simple command on the Terminal:
 - `python parking_prediction.py`